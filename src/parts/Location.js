import React from 'react';
import Title from '../components/Title';

const Location = ({ data }) => {
  return (
    <section
      className="location container-fluid d-flex flex-column align-items-center"
      data-aos="zoom-in"
      data-aos-duration="1500">
      <Title title="Location" />
      <div className="row mt-3">
        <div className="maps-1 col-md-6">
          <iframe
            className="mb-2"
            title="asi"
            src={data.maps}
            frameBorder="0"
            allowFullScreen=""
            aria-hidden="false"
            tabIndex="0"
          />
          <div className="h5">The first reception location</div>
        </div>
        <div className="col-md-6">
          <iframe
            className="mb-2"
            title="asi"
            src={`https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.97006925588!2d107.02652831476938!3d-6.267666795463437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3cea7ba6c52c336f!2zNsKwMTYnMDMuNiJTIDEwN8KwMDEnNDMuNCJF!5e0!3m2!1sen!2sid!4v1652452507882!5m2!1sen!2sid`}
            frameBorder="0"
            allowFullScreen=""
            aria-hidden="false"
            tabIndex="0"
          />
          <div className="h5">The second reception location</div>
        </div>
      </div>
    </section>
  );
};

export default Location;
